const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors('*'))
app.use(express.json())

// add the routers
const routerMovie = require('./routes/movie')
app.use('/movie', routerMovie)

//add server port
app.listen(4000, '0.0.0.0', () => {
  console.log('Movie  started on port 4000/4001 on local machine')
})