

CREATE TABLE Movie(
    movie_id Integer Primary Key auto_increment,
    movie_title VARCHAR(500),
    movie_release_date DATE,
    movie_time DATETIME,
    director_name VARCHAR(100)
);