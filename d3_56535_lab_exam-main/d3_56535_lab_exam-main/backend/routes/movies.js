const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router =express.Router()
//(movie_id, movie_title, movie_release_date,movie_time,director_name
router.post('/post',(request, response)=>{
    const {id,title,date,time,dname} = request.body

    const query = `INSERT INTO movie VALUES(${id},'${title}','${date}',${time},'${dname}')`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    }
    )
})
router.get('/getbyname',(request, response)=>{
    const {name} = request.body
    const query = `select * from movie where movie_title= '${name}'`
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    }
    )
})

router.patch('/update',(request, response)=>{
    const {id,date,time} = request.body
    const query = `update movie set movie_release_date='${date}',movie_time=${time} where movie_id=${id}`
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    }
    )
})

router.delete('/delete/:id',(request, response)=>{
    const id = request.params
    const query = `delete from movie where movie_id=${id}`
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    }
    )
})

module.exports = router