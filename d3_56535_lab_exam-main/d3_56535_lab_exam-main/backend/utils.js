const result = {}

module.exports.createResult = (error,data)=>{
    if(error){
        result.status = 'error'
        result.error = error
    }else{
        result.status = 'success'
        result.data = data
    }

    return result;
}