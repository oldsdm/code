create TABLE Movie(movie_id integer, movie_title VARCHAR(200), movie_release_date VARCHAR(200) ,movie_time VARCHAR(200),director_name VARCHAR(200)); 

INSERT INTO Movie (movie_id, movie_title,  movie_release_date, movie_time, director_name) VALUES(1, "Singham", "14-8-2020", "9.00 AM" , "Rohit_Shetty");
INSERT INTO Movie (movie_id, movie_title,  movie_release_date, movie_time, director_name) VALUES(2, "Pushpa", "14-8-2021", "9.00 AM" , "Rajnikant");
INSERT INTO Movie (movie_id, movie_title,  movie_release_date, movie_time, director_name) VALUES(3, "Don", "15-01-2022", "9.00 AM" , "Amitabh");