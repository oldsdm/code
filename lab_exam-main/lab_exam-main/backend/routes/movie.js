const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

// ADD A MOVIE TO A DATABASE

router.post("/", (request, response) => {
  const {
    movie_id,
    movie_title,
    movie_release_date,
    movie_time,
    director_name,
  } = request.body;

  const query = `
    INSERT INTO Movie
      (movie_id, movie_title,  movie_release_date, movie_time, director_name)
    VALUES
      (${movie_id},'${movie_title}','${movie_release_date}', '${movie_time}', '${director_name}')
  `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

// GET A MOVIE FROM DATABASE

router.get("/", (request, response) => {
  const query = `
    SELECT movie_id, movie_title,  movie_release_date, movie_time, director_name
    FROM Movie
  `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

//UPDATE MOVIE DETAILS

router.put("/:movie_id", (request, response) => {
  const { movie_id } = request.params;
  const { movie_release_date, movie_time } = request.body;

  const query = `
    UPDATE Movie
    SET
    movie_release_date = '${movie_release_date}', 
    movie_time = '${movie_time}'
    WHERE
    movie_id = ${movie_id}
  `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

//DELETE A MOVIE

router.delete("/:movie_id", (request, response) => {
  const { movie_id } = request.params;

  const query = `
    DELETE FROM Movie
    WHERE
    movie_id = ${movie_id}
  `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

module.exports = router;
